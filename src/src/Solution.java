import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public int[] twoSum(int[] nums, int target) {
        // Algo
        // Initialize HashMap<int, List<int>>
        // Hash all values with their index
        // Run for loop over each value, looking for target-num[i] in hashmap.
        // If found and i!= any index in map's list then this is a solution.
        // So, return index of element in map and element in for loop.
        // If not found, return empty array.

        int []result = new int[2];
        if(nums == null || nums.length<2) {
            return result;
        }
        Map<Integer, List<Integer>> map = new HashMap();
        for(int i=0; i<nums.length; i++) {
            if(map.containsKey(nums[i])) {
                List<Integer> indices = map.get(nums[i]);
                indices.add(i);
                map.put(nums[i], indices);
            } else {
                List<Integer> indices = new ArrayList<>();
                indices.add(i);
                map.put(nums[i], indices);
            }
        }
        for(int i=0; i<nums.length; i++) {
            int diff = target-nums[i];
            if(map.containsKey(diff)) {
                List<Integer> indices = map.get(diff);
                for(int index: indices) {
                    if(index != i) {
                        if(i<index) {
                            result[0] = i;
                            result[1] = index;
                        } else {
                            result[0] = index;
                            result[1] = i;
                        }
                        return result;
                    }
                }

            }
        }
        return result;
    }
}

// Test cases
// All numbers sum to target
// Extra numbers
// No instances of same number and double that is target
// Negative numbers to get to target
// Negative target
// No solution
// Null/Empty nums
